import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class IDataProviderMySQLImpl implements IDataProvider {

    private final String DB_URL = "jdbc:mysql://localhost:3306/sensors";
    private final String DB_USERNAME = "root";
    private final String DB_PASSWORD = "";

    @Override
    public List<String> getItems() {
        String sql = "select * from sensors.sensor_details";

        List<String> result = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(sql)
        ) {
            int resultSetColumnCount = resultSet.getMetaData().getColumnCount();
            while (resultSet.next()) {
                for (int i = 1; i <= resultSetColumnCount; i++) {
                    result.add(String.valueOf(resultSet.getObject(i)));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    @Override
    public String getItem() {
        String sql = "select * from sensors.sensor_details where sensor_id = ?";
        String result = "";

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            preparedStatement.setInt(1, 1);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    result = resultSet.getString("sensor_name");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return result;
    }

    @Override
    public void createItem() throws SQLException {
        String sql = "insert into `sensors`.sensor_details(`sensor_id`, `sensor_name`) values (4, 'D')";

        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        Statement statement = connection.createStatement();
        int a = statement.executeUpdate(sql);

        connection.close();
    }

    @Override
    public void deleteItem() throws SQLException {
        String sql = "delete from sensors.sensor_details where sensor_id = 4";

        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        Statement statement = connection.createStatement();
        int a = statement.executeUpdate(sql);

        connection.close();
    }

    @Override
    public void saveChanges() throws SQLException {
        String sql = "update sensors.sensor_details set sensor_name = 'D' where sensor_id = 3";

        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        Statement statement = connection.createStatement();
        int a = statement.executeUpdate(sql);

        connection.close();
    }

    @Override
    public List<String> getItemsOfSensor(int sensorId) {
        String sql = "SELECT sensor_name, dti.data, dti.effective_date, dtd.data, dtd.effective_date, " +
                "dtb.data, dtb.effective_date, dts.data, dts.effective_date " +
                "FROM sensors.sensor_details e " +
                "LEFT JOIN sensors.data_type_integer dti on e.sensor_id = dti.sensor_id " +
                "LEFT JOIN sensors.data_type_double dtd on e.sensor_id = dtd.sensor_id " +
                "LEFT JOIN sensors.data_type_boolean dtb on e.sensor_id = dtb.sensor_id " +
                "LEFT JOIN sensors.data_type_string dts on e.sensor_id = dts.sensor_id " +
                "WHERE e.sensor_id = ?";


        List<String> result = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
             PreparedStatement preparedStatement = connection.prepareStatement(sql);

        ) {
            preparedStatement.setInt(1, sensorId);

            try(ResultSet resultSet = preparedStatement.executeQuery()) {
                int resultSetColumnCount = resultSet.getMetaData().getColumnCount();
                while (resultSet.next()){
                    for (int i = 1; i <= resultSetColumnCount; i++) {
                        result.add(String.valueOf(resultSet.getObject(i)));
                    }
                }
            }

            } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
        return result;
    }

}
