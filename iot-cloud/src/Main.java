import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        IDataProvider iDataProvider = new IDataProviderMySQLImpl();

//        iDataProvider.getItems().forEach(e -> System.out.println(e));
//        System.out.println(iDataProvider.getItem());
//        iDataProvider.createItem();
//        iDataProvider.deleteItem();
//        iDataProvider.saveChanges();
        iDataProvider.getItemsOfSensor(1).forEach(e -> System.out.println(e));
    }
}
