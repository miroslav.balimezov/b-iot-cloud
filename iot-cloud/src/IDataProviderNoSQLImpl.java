import com.mongodb.*;

import com.mongodb.MongoCredential;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

public class IDataProviderNoSQLImpl {


    public static void main(String args[]) throws UnknownHostException {

        // Creating a Mongo client
        MongoClient mongoClient = new MongoClient(new MongoClientURI("mongodb://localhost:27017"));


//        MongoClient mongo = new MongoClient("localhost", 27017);

        // Creating Credentials
        MongoCredential credential = MongoCredential.createMongoCRCredential("sampleUser", "products", "password".toCharArray());
        System.out.println("Connected to the database successfully");

        // Accessing the database
        DB database = mongoClient.getDB("products");
        System.out.println("Credentials ::" + credential);

        // Print names of Databases
        List dbs = mongoClient.getDatabaseNames();
        for(Object db : dbs) {
            System.out.println(db);
        }




        DBCollection coll = database.getCollection("products");
        Set colls = database.getCollectionNames();
        for (Object s : colls) {
            System.out.println(s);
        }



        BasicDBObject query = new BasicDBObject("i", 52);
        DBCursor cursor = coll.find(query);
        while(cursor.hasNext()) {
            System.out.println(cursor.next());
        }
        cursor.close();




        mongoClient.close();
    }
}
