import java.sql.SQLException;
import java.util.List;

public interface IDataProvider {

    List<String> getItems();

    String getItem();

    void createItem() throws SQLException;

    void deleteItem() throws SQLException;

    void saveChanges() throws SQLException;

    List<String> getItemsOfSensor(int sensorId);

}
