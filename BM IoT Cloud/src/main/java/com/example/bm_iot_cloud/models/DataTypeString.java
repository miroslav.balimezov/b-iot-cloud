package com.example.bm_iot_cloud.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "data_type_string")@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class DataTypeString extends BaseEntity{

    @Column(name = "data")
    private String data;

    @Column(name = "effective_date")
    private Timestamp timestamp;

    @ManyToOne
    @JoinColumn(name = "sensor_id")
    private SensorDetails sensorDetails;
}
