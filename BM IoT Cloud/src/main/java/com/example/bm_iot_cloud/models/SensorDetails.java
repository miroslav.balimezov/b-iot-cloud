package com.example.bm_iot_cloud.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Table(name = "sensor_details")
@NoArgsConstructor
@Getter
@Setter
public class SensorDetails extends BaseEntity{

    @NotBlank
    @Column(name = "sensor_name")
    @Size(min = 4, max = 50, message = "Addon's name must be between 4 and 50 characters!")
    private String name;
}
