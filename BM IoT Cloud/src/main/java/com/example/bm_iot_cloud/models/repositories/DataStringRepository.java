package com.example.bm_iot_cloud.models.repositories;

import com.example.bm_iot_cloud.models.DataTypeString;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataStringRepository extends JpaRepository<DataTypeString, Integer> {

    DataTypeString findById(int id);

    List<DataTypeString> findAll();

    List<DataTypeString> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeString> findAllBySensorDetails_Name(String sensorName);

}
