package com.example.bm_iot_cloud.models.repositories;

import com.example.bm_iot_cloud.models.DataTypeDouble;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataDoubleRepository extends JpaRepository<DataTypeDouble, Integer> {

    DataTypeDouble findById(int id);

    List<DataTypeDouble> findAll();

    List<DataTypeDouble> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeDouble> findAllBySensorDetails_Name(String sensorName);

}
