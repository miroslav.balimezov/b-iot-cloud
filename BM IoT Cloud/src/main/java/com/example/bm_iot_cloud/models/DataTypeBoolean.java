package com.example.bm_iot_cloud.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "data_type_boolean")
@NoArgsConstructor
@Getter
@Setter
public class DataTypeBoolean extends BaseEntity{

    @Column(name = "data")
    private boolean data;

    @Column(name = "effective_date")
    private Timestamp timestamp;

    @ManyToOne
    @JoinColumn(name = "sensor_id")
    private SensorDetails sensorDetails;
}
