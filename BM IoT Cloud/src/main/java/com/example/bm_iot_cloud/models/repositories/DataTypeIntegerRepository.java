package com.example.bm_iot_cloud.models.repositories;

import com.example.bm_iot_cloud.models.DataTypeInteger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataTypeIntegerRepository extends JpaRepository<DataTypeInteger, Integer> {

    DataTypeInteger findById(int id);

    List<DataTypeInteger> findAll();

    List<DataTypeInteger> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeInteger> findAllBySensorDetails_Name(String sensorName);

}
