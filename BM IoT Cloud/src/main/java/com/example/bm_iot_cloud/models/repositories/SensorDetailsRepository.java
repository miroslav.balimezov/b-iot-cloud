package com.example.bm_iot_cloud.models.repositories;

import com.example.bm_iot_cloud.models.SensorDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SensorDetailsRepository extends JpaRepository<SensorDetails, Integer> {

    SensorDetails findById(int id);

    List<SensorDetails> findAll();

    SensorDetails findByName(String name);

}
