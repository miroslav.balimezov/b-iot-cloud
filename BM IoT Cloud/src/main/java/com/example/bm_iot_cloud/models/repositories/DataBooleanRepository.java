package com.example.bm_iot_cloud.models.repositories;

import com.example.bm_iot_cloud.models.DataTypeBoolean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DataBooleanRepository extends JpaRepository<DataTypeBoolean, Integer> {

    DataTypeBoolean findById(int id);

    List<DataTypeBoolean> findAll();

    List<DataTypeBoolean> findAllBySensorDetails_Id(int sensorId);

    List<DataTypeBoolean> findAllBySensorDetails_Name(String sensorName);


}
