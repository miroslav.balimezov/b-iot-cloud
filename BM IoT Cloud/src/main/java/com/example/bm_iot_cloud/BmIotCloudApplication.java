package com.example.bm_iot_cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BmIotCloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(BmIotCloudApplication.class, args);
    }

}
